/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.repo.db;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@EnableWebMvc
@CrossOrigin(maxAge = 3600)
public class HTMLResolver extends WebMvcConfigurerAdapter {

  @Bean
  public ViewResolver getViewResolver() {
    InternalResourceViewResolver resolver = new InternalResourceViewResolver();
    resolver.setPrefix("/WEB-INF/");
    resolver.setSuffix("*.html");
    return resolver;
  }

  @Override
  public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
    configurer.enable();
  }

  @ExceptionHandler(NoHandlerFoundException.class)
  public String handleError404() {
    return "redirect:/index.html";
  }

  @Override
  public void addViewControllers(ViewControllerRegistry registry) {
    registry.addViewController("/").setViewName("forward:/index.html");
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    System.out.println(registry.toString());
    registry.addResourceHandler("/**").addResourceLocations("/resources/");
  }

  /*
   * @Bean public EmbeddedServletContainerCustomizer containerCustomizer() {
   * return container -> { container.addErrorPages(new
   * ErrorPage(HttpStatus.NOT_FOUND, "/notFound")); }; }
   */

}