package com.repo.db;

import com.repo.utils.PermissionException;
import com.repo.utils.UnauthenticatedException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(Exception.class)
  public ResponseEntity<com.repo.utils.Response> generalException(Exception ex, HttpServletRequest req) {
    com.repo.utils.Response res = new com.repo.utils.Response(req, HttpStatus.INTERNAL_SERVER_ERROR, ex);
    return new ResponseEntity<>(res, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(UnauthenticatedException.class)
  public ResponseEntity<com.repo.utils.Response> unauthenticatedException(UnauthenticatedException ex,
                                                                          HttpServletRequest req) {
    com.repo.utils.Response res = new com.repo.utils.Response(req, HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
    return new ResponseEntity<>(res, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(PermissionException.class)
  public ResponseEntity<com.repo.utils.Response> permissionException(PermissionException ex, HttpServletRequest req) {
    com.repo.utils.Response res = new com.repo.utils.Response(req, HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
    return new ResponseEntity<>(res, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(value = {IllegalArgumentException.class, IllegalStateException.class})
  protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
    String bodyOfResponse = "This should be application specific";
    return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.CONFLICT, request);
  }
}
