package com.repo.db.model;

import javax.persistence.*;

@Entity
@Table(name = "Repository")
public class Repo {
  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(name = "title")
  private String title;

  @Column(name = "url", nullable = true)
  private String url;

  @Column(name = "topic")
  private Integer topic;

  @Column(name = "type")
  private Integer type;

  @Column(name = "createdAt")
  @GeneratedValue(strategy = GenerationType.AUTO)
  private String createdAt;

  @Column(name = "tags", nullable = true)
  private String tags;

  @Column(name = "username")
  private String username;

  @Column(name = "idUser")
  private Integer idUser;

  public Integer getIdUser() {
    return idUser;
  }

  /**
   * @param idUser the idUser to set
   */
  public void setIdUser(Integer idUser) {
    this.idUser = idUser;
  }

  public Repo beforeCreate(User user) {
    this.id = null;
    this.username = user.getUsername();
    this.idUser = user.getId();
    return this;
  }

  public Repo beforeUpdate(Integer id, User user) {
    this.id = id;
    this.username = user.getUsername();
    this.idUser = user.getId();
    return this;
  }

  private Integer visibility;

  @Column(nullable = true)
  private String file;

  @Column(nullable = true)
  private String image;

  @Column(nullable = true)
  private String description;

  /**
   * @return the id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * @param title the title to set
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * @return the url
   */
  public String getUrl() {
    return url;
  }

  /**
   * @param url the url to set
   */
  public void setUrl(String url) {
    this.url = url;
  }

  /**
   * @return the topic
   */
  public Integer getTopic() {
    return topic;
  }

  /**
   * @param topic the topic to set
   */
  public void setTopic(Integer topic) {
    this.topic = topic;
  }

  /**
   * @return the type
   */
  public Integer getType() {
    return type;
  }

  /**
   * @param type the type to set
   */
  public void setType(Integer type) {
    this.type = type;
  }

  /**
   * @return the createdAt
   */
  public String getCreatedAt() {
    return createdAt;
  }

  /**
   * @param createdAt the createdAt to set
   */
  public void setCreatedAt(String createdAt) {
    this.createdAt = createdAt;
  }

  /**
   * @return the tags
   */
  public String getTags() {
    return tags;
  }

  /**
   * @param tags the tags to set
   */
  public void setTags(String tags) {
    this.tags = tags;
  }

  /**
   * @return the username
   */
  public String getUsername() {
    return username;
  }

  /**
   * @param username the username to set
   */
  public void setUsername(String username) {
    this.username = username;
  }

  /**
   * @return the visibility
   */
  public Integer getVisibility() {
    return visibility;
  }

  /**
   * @param visibility the visibility to set
   */
  public void setVisibility(Integer visibility) {
    this.visibility = visibility;
  }

  /**
   * @return the file
   */
  public String getFile() {
    return file;
  }

  /**
   * @param file the file to set
   */
  public void setFile(String file) {
    this.file = file;
  }

  /**
   * @return the image
   */
  public String getImage() {
    return image;
  }

  /**
   * @param image the image to set
   */
  public void setImage(String image) {
    this.image = image;
  }

  public void setNullId() {
    this.setId(null);
  }

  /**
   * @return the description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description the description to set
   */
  public void setDescription(String description) {
    this.description = description;
  }
}
