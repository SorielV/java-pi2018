package com.repo.db.model.catalog;

import com.repo.db.model.User;
import com.repo.utils.Methods;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "Types")
@Table(name = "CatTypes")
public class Types implements Serializable, Methods {
  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(name = "text")
  private String text;

  @Column(name = "description")
  private String description;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Types() {
  }

  ;

  @Override
  public Types beforeUpdate(Integer id, User user) {
    this.id = id;
    return null;
  }

  @Override
  public Types beforeCreate(User user) {
    this.id = null;
    return null;
  }

  @Override
  public Types validation() {
    return null;
  }
}