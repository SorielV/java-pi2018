package com.repo.db.model;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "User")
public class User implements Serializable {
  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(name = "username", unique = true)
  private String username;

  @Column(name = "email", unique = true)
  private String email;

  @Column(name = "password")
  private String password;

  @Column(name = "firstName")
  private String firstName;

  @Column(name = "lastName")
  private String lastName;

  @Column(name = "birthDate")
  private String birthDate;

  @Column(name = "profileImage", nullable = true)
  private String profileImage;

  @Column(name = "userType", nullable = true)
  private Integer userType;

  @Column(name = "recoveryToken", nullable = true)
  private String recoveryToken;

  @Column(name = "emailToken", nullable = true)
  private String emailToken;

  @Column(name = "createdAt", updatable = false, nullable = true)
  private String createdAt;

  @Column(name = "updatedAt", updatable = false, nullable = true)
  private String updatedAt;

  public User() {
  }

  public User(Integer id, String username, Integer userType) {
    this.id = id;
    this.username = username;
    this.userType = userType;
  }

  public User beforeCreate() {
    this.id = null;
    return this;
  }

  public User beforeUpdate() {
    return this;
  }

  public void setNullId() {
    this.setId(null);
  }

  /**
   * @return the id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * @return the firstName
   */
  public String getFirstName() {
    return firstName;
  }

  /**
   * @param firstName the firstName to set
   */
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  /**
   * @return the lastName
   */
  public String getLastName() {
    return lastName;
  }

  /**
   * @param lastName the lastName to set
   */
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  /**
   * @return the birthDate
   */
  public String getBirthDate() {
    return birthDate;
  }

  /**
   * @param birthDate the birthDate to set
   */
  public void setBirthDate(String birthDate) {
    this.birthDate = birthDate;
  }

  /**
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * @return the userType
   */
  public Integer getUserType() {
    return userType;
  }

  /**
   * @param userType the userType to set
   */
  public void setUserType(Integer userType) {
    this.userType = userType;
  }

  /**
   * @return the recoveryToken
   */
  public String getRecoveryToken() {
    return recoveryToken;
  }

  /**
   * @param recoveryToken the recoveryToken to set
   */
  public void setRecoveryToken(String recoveryToken) {
    this.recoveryToken = recoveryToken;
  }

  /**
   * @return the emailToken
   */
  public String getEmailToken() {
    return emailToken;
  }

  /**
   * @param emailToken the emailToken to set
   */
  public void setEmailToken(String emailToken) {
    this.emailToken = emailToken;
  }

  /**
   * @return the createdAt
   */
  public String getCreatedAt() {
    return createdAt;
  }

  /**
   * @param createdAt the createdAt to set
   */
  public void setCreatedAt(String createdAt) {
    this.createdAt = createdAt;
  }

  /**
   * @return the username
   */
  public String getUsername() {
    return username;
  }

  /**
   * @param username the username to set
   */
  public void setUsername(String username) {
    this.username = username;
  }

  public String getProfileImage() {
    return profileImage;
  }

  public void setProfileImage(String profileImage) {
    this.profileImage = profileImage;
  }

  /**
   * @return the updatedAt
   */
  public String getUpdatedAt() {
    return updatedAt;
  }

  /**
   * @param updatedAt the updatedAt to set
   */
  public void setUpdatedAt(String updatedAt) {
    this.updatedAt = updatedAt;
  }

  /**
   * @return the password
   */
  public String getPassword() {
    return password;
  }

  /**
   * @param password the password to set
   */
  public void setPassword(String password) {
    this.password = password;
  }
}
