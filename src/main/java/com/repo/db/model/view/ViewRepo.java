package com.repo.db.model.view;

import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Immutable
@Table(name = "Repository")
// @SecondaryTable(name = "viewRepositoryAuthor", pkJoinColumns =
// @PrimaryKeyJoinColumn(name = "idRepository", referencedColumnName = "id"))
public class ViewRepo implements Serializable {

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private String title;
  private String url;
  private Integer topic;
  private Integer type;
  private String createdAt;
  private String tags;
  private String username;
  private Integer userId;
  private Integer visibility;
  private String file;
  private String image;
  private String description;

  @OneToMany(targetEntity = ViewRepoAuthor.class, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
  @JoinColumn(name = "idRepository", referencedColumnName = "id")
  private List<ViewRepoAuthor> author;

  /*
   * @OneToMany
   *
   * @JoinTable( name = "viewRepositoryAuthor", joinColumns = @JoinColumn(name =
   * "idRepository") ) public List<ViewRepositoryAuthor> viewRepositoryAuthor;
   */

  public ViewRepo() {
  }

  public Integer getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }

  public String getUrl() {
    return url;
  }

  public Integer getTopic() {
    return topic;
  }

  public Integer getType() {
    return type;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public String getTags() {
    return tags;
  }

  public String getUsername() {
    return username;
  }

  public Integer getUserId() {
    return userId;
  }

  public Integer getVisibility() {
    return visibility;
  }

  public String getFile() {
    return file;
  }

  public String getImage() {
    return image;
  }

  public String getDescription() {
    return description;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public void setTopic(Integer topic) {
    this.topic = topic;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public void setCreatedAt(String createdAt) {
    this.createdAt = createdAt;
  }

  public void setTags(String tags) {
    this.tags = tags;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public void setVisibility(Integer visibility) {
    this.visibility = visibility;
  }

  public void setFile(String file) {
    this.file = file;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * @return the authorsx
   */
  public List<ViewRepoAuthor> getAuthor() {
    return author;
  }

  /**
   * @param authorsx the authorsx to set
   */
  public void setAuthor(List<ViewRepoAuthor> authors) {
    this.author = authors;
  }
}
