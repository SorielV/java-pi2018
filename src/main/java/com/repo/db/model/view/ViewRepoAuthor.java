package com.repo.db.model.view;

import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "ViewRepoAuthor")
public class ViewRepoAuthor {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(name = "idAuthor")
  private Integer idAuthor;

  @Column(name = "idRepository")
  private Integer idRepository;

  @Column(name = "firstName")
  private String firstName;

  @Column(name = "lastName")
  private String lastName;

  /*
   * @ManyToOne private ViewRepo viewRepo;
   */

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getIdRepository() {
    return idRepository;
  }

  public void setIdRepository(Integer idRepository) {
    this.idRepository = idRepository;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  /**
   * @return the idAuthor
   */
  public Integer getIdAuthor() {
    return idAuthor;
  }

  /**
   * @param idAuthor the idAuthor to set
   */
  public void setIdAuthor(Integer idAuthor) {
    this.idAuthor = idAuthor;
  }
}
