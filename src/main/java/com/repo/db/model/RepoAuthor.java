package com.repo.db.model;

import javax.persistence.*;

@Entity
@Table(name = "RepoAuthor")
public class RepoAuthor {
  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(name = "idAuthor")
  private Integer idAuthor;

  @Column(name = "idRepository")
  private Integer idRepository;

  public RepoAuthor beforeCreate(Integer idRepository) {
    this.idRepository = idRepository;
    return this;
  }

  /**
   * @return the id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * @return the idAuthor
   */
  public Integer getIdAuthor() {
    return idAuthor;
  }

  /**
   * @param idAuthor the idAuthor to set
   */
  public void setIdAuthor(Integer idAuthor) {
    this.idAuthor = idAuthor;
  }

  /**
   * @return the idRepository
   */
  public Integer getIdRepository() {
    return idRepository;
  }

  /**
   * @param idRepository the idRepository to set
   */
  public void setIdRepository(Integer idRepository) {
    this.idRepository = idRepository;
  }
}
