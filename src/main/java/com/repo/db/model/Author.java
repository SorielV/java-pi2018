package com.repo.db.model;

import com.repo.utils.Methods;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "Author")
@Table(name = "Author")
public class Author implements Serializable, Methods {
  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(name = "firstName")
  private String firstName;

  @Column(name = "lastName")
  private String lastName;

  @Column(name = "createdAt", nullable = true)
  private String createdAt;

  public Author beforeCreate() {
    this.id = null;
    return this;
  }

  public Author beforeUpdate(int id) {
    this.id = id;
    return this;
  }

  /**
   * @return the id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * @return the firstName
   */
  public String getFirstName() {
    return firstName;
  }

  /**
   * @param firstName the firstName to set
   */
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  /**
   * @return the lastName
   */
  public String getLastName() {
    return lastName;
  }

  /**
   * @param lastName the lastName to set
   */
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  /**
   * @return the createdAt
   */
  public String getCreatedAt() {
    return createdAt;
  }

  /**
   * @param createdAt the createdAt to set
   */
  @Transient
  public void setCreatedAt(String createdAt) {
    this.createdAt = createdAt;
  }

  @Override
  public Object beforeUpdate(Integer id, User user) {
    this.id = id;
    return this;
  }

  @Override
  public Object beforeCreate(User user) {
    this.id = null;
    return this;
  }

  @Override
  public Object validation() {
    return null;
  }
}