package com.repo.db.repository;

import com.repo.db.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User, Integer> {
  public User findById(Integer id);

  @Query("SELECT u FROM User u WHERE u.username = :username AND password = :password")
  public User findByUsernameAndPassword(@Param("username") String username, @Param("password") String password);
}
