package com.repo.db.repository;

import com.repo.db.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author, Integer> {
  public Author findById(Integer id);
}