package com.repo.db.repository;

import com.repo.db.model.Repo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepoRepository extends JpaRepository<Repo, Integer> {
  public Repo findById(Integer id);
}
