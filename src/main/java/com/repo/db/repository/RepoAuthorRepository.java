package com.repo.db.repository;

import com.repo.db.model.RepoAuthor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepoAuthorRepository extends JpaRepository<RepoAuthor, Integer> {

}
