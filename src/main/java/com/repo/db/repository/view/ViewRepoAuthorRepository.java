package com.repo.db.repository.view;

import com.repo.db.model.view.ViewRepoAuthor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ViewRepoAuthorRepository extends JpaRepository<ViewRepoAuthor, Integer> {
  public ViewRepoAuthor findById(int id);

  public ViewRepoAuthor findByIdRepository(int idRepository);

}
