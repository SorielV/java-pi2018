package com.repo.db.repository.view;

import com.repo.db.model.view.ViewRepo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ViewRepoRepository extends JpaRepository<ViewRepo, Integer> {
  public ViewRepo findById(int id);

  public List<ViewRepo> findByType(int type);
  public List<ViewRepo> findByTopic(int topic);
}
