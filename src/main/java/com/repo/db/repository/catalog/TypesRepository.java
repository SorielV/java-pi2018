package com.repo.db.repository.catalog;

import com.repo.db.model.catalog.Types;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TypesRepository extends JpaRepository<Types, Integer> {

}
