package com.repo.db.resource.catalog;

import com.repo.db.model.catalog.Types;
import com.repo.utils.BasicCRUD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/api/type")
public class TypesResource extends BasicCRUD<Types, Integer> {
  @Autowired
  public TypesResource(CrudRepository<Types, Integer> repository, HttpServletRequest req, HttpServletResponse res) {
    super(repository, req, res);
  }
}
