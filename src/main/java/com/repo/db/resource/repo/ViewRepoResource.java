package com.repo.db.resource.repo;

import com.repo.db.repository.view.ViewRepoRepository;
import com.repo.utils.PermissionException;
import com.repo.utils.RequestHandle;
import com.repo.utils.Response;
import com.repo.utils.UnauthenticatedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin(maxAge = 3600)
@RestController
public class ViewRepoResource extends RequestHandle<ViewRepoRepository> {
  @Autowired
  public ViewRepoResource(ViewRepoRepository repository, HttpServletRequest req, HttpServletResponse res) {
    super(repository, req, res);
  }

  @GetMapping(value = "/api/repo")
  @ResponseStatus(code = HttpStatus.OK)
  public Response getAll(HttpServletRequest req, HttpServletResponse res)
          throws UnauthenticatedException, PermissionException {
    handleAuthCookieBase(false, false);
    return new Response(req, HttpStatus.OK, repository.findAll());
  }

  @GetMapping(value = "/api/repo/topic/{id}")
  @ResponseStatus(code = HttpStatus.OK)
  public Response getByTopic(@PathVariable("id") int id, HttpServletRequest req, HttpServletResponse res)
          throws UnauthenticatedException, PermissionException {
    handleAuthCookieBase(false, false);
    return new Response(req, HttpStatus.OK, repository.findByTopic(id));
  }

  @GetMapping(value = "/api/repo/type/{id}")
  @ResponseStatus(code = HttpStatus.OK)
  public Response getByType(@PathVariable("id") int id, HttpServletRequest req, HttpServletResponse res)
          throws UnauthenticatedException, PermissionException {
    handleAuthCookieBase(false, false);
    return new Response(req, HttpStatus.OK, repository.findByType(id));
  }

  @GetMapping(value = "/api/repo/{id}")
  @ResponseStatus(code = HttpStatus.OK)
  public Response getById(@PathVariable("id") int id, HttpServletRequest req, HttpServletResponse res)
          throws UnauthenticatedException, PermissionException {
    handleAuthCookieBase(false, false);
    return new Response(req, HttpStatus.OK, repository.findById(id));
  }
}
