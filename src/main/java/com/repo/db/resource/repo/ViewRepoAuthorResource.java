package com.repo.db.resource.repo;

import com.repo.db.repository.view.ViewRepoAuthorRepository;
import com.repo.utils.PermissionException;
import com.repo.utils.RequestHandle;
import com.repo.utils.Response;
import com.repo.utils.UnauthenticatedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin(maxAge = 3600)
@RestController
public class ViewRepoAuthorResource extends RequestHandle<ViewRepoAuthorRepository> {
  @Autowired
  public ViewRepoAuthorResource(ViewRepoAuthorRepository repository, HttpServletRequest req, HttpServletResponse res) {
    super(repository, req, res);
  }

  @GetMapping(value = "/api/repo/{idRepository}/author")
  @ResponseStatus(code = HttpStatus.OK)
  public Response getAll(@PathVariable("idRepository") int idRepository)
          throws UnauthenticatedException, PermissionException {
    handleAuthCookieBase(true, true);
    return new Response(req, HttpStatus.OK, repository.findAll());
  }

  @GetMapping(value = "/api/repo/{idRepository}/author/{idRepoAuthor}")
  @ResponseStatus(code = HttpStatus.OK)
  public Response getById(@PathVariable("idRepository") int idRepository,
                          @PathVariable("idRepoAuthor") int idRepoAuthor) throws UnauthenticatedException, PermissionException {
    handleAuthCookieBase(true, true);
    return new Response(req, HttpStatus.OK, repository.findByIdRepository(idRepository));
  }
}
