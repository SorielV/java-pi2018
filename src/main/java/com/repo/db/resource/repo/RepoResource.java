package com.repo.db.resource.repo;

import com.repo.db.model.Repo;
import com.repo.db.repository.RepoRepository;
import com.repo.utils.PermissionException;
import com.repo.utils.RequestHandle;
import com.repo.utils.Response;
import com.repo.utils.UnauthenticatedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.config.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin(maxAge = 3600)
@RestController
public class RepoResource extends RequestHandle<RepoRepository> {
  @Autowired
  public RepoResource(RepoRepository repository, HttpServletRequest req, HttpServletResponse res) {
    super(repository, req, res);
  }

  @PostMapping(value = "/api/repo")
  @ResponseStatus(code = HttpStatus.CREATED)
  public Response create(@RequestBody final Repo model) throws UnauthenticatedException, PermissionException {
    handleAuthCookieBase(true, true);
    return new Response(req, HttpStatus.CREATED, repository.save(model.beforeCreate(user.getUser())));
  }

  @PutMapping(value = "/api/repo/{id}")
  @ResponseStatus(code = HttpStatus.OK)
  public Response update(@PathVariable("id") int id, @RequestBody final Repo commit)
          throws PermissionException, UnauthenticatedException {
    handleAuthCookieBase(true, true);
    return new Response(req, HttpStatus.OK, repository.save(commit.beforeUpdate(id, user.getUser())));
  }

  @DeleteMapping(value = "/api/repo/{id}")
  public Response delete(@PathVariable("id") int id)
          throws UnauthenticatedException, PermissionException, ResourceNotFoundException {
    handleAuthCookieBase(true, true);
    // Repo repo = repository.findById(id);
    repository.delete(id);
    return new Response(req, HttpStatus.NO_CONTENT, null);
  }
}
