package com.repo.db.resource.repo;

import com.repo.db.model.RepoAuthor;
import com.repo.db.repository.RepoAuthorRepository;
import com.repo.utils.PermissionException;
import com.repo.utils.RequestHandle;
import com.repo.utils.Response;
import com.repo.utils.UnauthenticatedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin(maxAge = 3600)
@RestController
public class RepoAuthorResoruce extends RequestHandle<RepoAuthorRepository> {
  @Autowired
  public RepoAuthorResoruce(RepoAuthorRepository repository, HttpServletRequest req, HttpServletResponse res) {
    super(repository, req, res);
  }

  @PostMapping(value = "/api/repo/{idRepository}/author")
  @ResponseStatus(code = HttpStatus.CREATED)
  public Response createRepoAuthor(@PathVariable("idRepository") int idRepository, @RequestBody final RepoAuthor commit)
          throws UnauthenticatedException, PermissionException {
    handleAuthCookieBase(true, true);
    return new Response("", 201, repository.save(commit.beforeCreate(idRepository)));
  }
}
