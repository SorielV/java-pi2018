package com.repo.db.resource.author;

import com.repo.db.model.Author;
import com.repo.db.repository.AuthorRepository;
import com.repo.utils.PermissionException;
import com.repo.utils.RequestHandle;
import com.repo.utils.Response;
import com.repo.utils.UnauthenticatedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.config.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/api/author")
public class AuthorResource extends RequestHandle<AuthorRepository> {
  @Autowired
  public AuthorResource(AuthorRepository repository, HttpServletRequest req, HttpServletResponse res)
          throws UnauthenticatedException, PermissionException {
    super(repository, req, res);
  }

  @GetMapping(value = "")
  @ResponseStatus(code = HttpStatus.OK)
  public Response getAll() throws UnauthenticatedException, PermissionException {
    handleAuth(true, true);
    return new Response(req, HttpStatus.OK, repository.findAll());
  }

  @PostMapping(value = "")
  @ResponseStatus(code = HttpStatus.CREATED)
  public Response create(@RequestBody final Author model)
          throws IllegalArgumentException, UnauthenticatedException, PermissionException {
    handleAuth(true, true);
    return new Response(req, HttpStatus.CREATED, repository.save(model.beforeCreate()));
  }

  @PutMapping(value = "/{id}")
  @ResponseStatus(code = HttpStatus.OK)
  public Response update(@PathVariable("id") int id, @RequestBody final Author commit)
          throws IllegalArgumentException, UnauthenticatedException, PermissionException {
    handleAuth(true, true);
    return new Response(req, HttpStatus.OK, repository.save(commit.beforeUpdate(id)));
  }

  @DeleteMapping(value = "")
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public Response delete(@PathVariable("id") int id)
          throws ResourceNotFoundException, UnauthenticatedException, PermissionException {
    handleAuthCookieBase(true, true); // Author autor = repository.findById(id);
    repository.delete(id);
    return new Response(req, HttpStatus.NO_CONTENT, null);
  }
}