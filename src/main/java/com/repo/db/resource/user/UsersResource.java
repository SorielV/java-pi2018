package com.repo.db.resource.user;

import com.repo.db.model.User;
import com.repo.db.repository.UserRepository;
import com.repo.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;

@CrossOrigin(maxAge = 3600)
@RestController
public class UsersResource extends RequestHandle<UserRepository> {
  @Autowired
  public UsersResource(UserRepository repository, HttpServletRequest req, HttpServletResponse res) {
    super(repository, req, res);
  }

  @GetMapping(value = "/api/user")
  @ResponseStatus(code = HttpStatus.OK)
  public Response getAll() throws UnauthenticatedException, PermissionException {
    handleAuth(true, true);
    return new Response(req, HttpStatus.OK, repository.findAll());
  }

  @PostMapping(value = "/api/user")
  @ResponseStatus(code = HttpStatus.CREATED)
  public Response persist(@RequestBody final User user) {
    return new Response(req, HttpStatus.CREATED, repository.save(user.beforeCreate()));
  }

  /*@PostMapping(value = "/login")
  @ResponseStatus(code = HttpStatus.OK)
  public Response login(@RequestPart(value = "username", required = true) String username,
                        @RequestPart(value = "password", required = true) String password)
          throws UnauthenticatedException, PermissionException, UnsupportedEncodingException {
    handleAuth(false, false);
    if (user != null)
      return new Response(req, 403, "Ya has ingresado");
    User user = repository.findByUsernameAndPassword(username, password);
    if (user != null) {
      String token = Auth.generateJWT(user);
    }

    res.setStatus(user != null ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
    return user != null ? new Response(req, HttpStatus.OK, user)
            : new Response(req, HttpStatus.BAD_REQUEST, "Usuario o password incorrecto");
  }*/

  @PostMapping(value = "/login")
  @ResponseStatus(code = HttpStatus.OK)
  public AuthUser login(@RequestBody Credentials credentials)
          throws UnauthenticatedException, PermissionException, UnsupportedEncodingException {
    handleAuth(false, false);
    if (user != null) return user;

    User user = repository.findByUsernameAndPassword(credentials.username, credentials.password);
    String token = null;
    if (user != null) {
      token = Auth.generateJWT(user);
    }

    res.setStatus(user != null ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
    return user != null ? new AuthUser(user, token) : null;
  }

  // Authorization: Bearer <token>
  @GetMapping(value = "/login/sv")
  @ResponseStatus(code = HttpStatus.OK)
  public Response login() throws UnauthenticatedException, PermissionException, UnsupportedEncodingException {
    User user = repository.findByUsernameAndPassword("Soriel", "Hacks");
    final String token = Auth.generateJWT(user);
    res.setHeader("Authorization", "Bearer " + token);
    return new Response(req, HttpStatus.OK, new AuthUser(user, token));
  }

  @GetMapping(value = "/login/status")
  @ResponseStatus(code = HttpStatus.OK)
  public AuthUser statusLogin() throws UnsupportedEncodingException, UnauthenticatedException, PermissionException {
    handleAuth(false, true);
    return user;
  }

}

/*
 * String jws = Jwts.builder() .claim("username", user.getUsername())
 * .claim("isAdmin", user.getUserType() == 1) .claim("name", user.getFirstName()
 * + " " + user.getLastName())
 * .setIssuedAt(Date.from(Instant.ofEpochSecond(1466796822L)))
 * .setExpiration(Date.from(Instant.ofEpochSecond(4622470422L))) .signWith(
 * SignatureAlgorithm.HS256,
 * "Yn2kjibddFAWtnPJ2AFlL8WXmohJMCvigQggaEypa5E=".getBytes("UTF-8") )
 * .compact(); System.out.println(jws); Response response = new Response("",
 * 200, jws); /*
 *
 * @GetMapping(value = "/login/sv") public Response login() throws
 * UnauthenticatedException, PermissionException { handleAuthCookieBase(false,
 * false); if (user != null) return new Response(req, 403, "Ya has ingresado");
 * User user = repository.findByUsernameAndPassword("Soriel", "Hacks"); if (user
 * != null) { HttpSession session = req.getSession(); user.setPassword(null);
 * session.setAttribute("user", user); } res.setStatus(user != null ?
 * HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value()); return user != null
 * ? new Response(req, HttpStatus.OK, user) : new Response(req,
 * HttpStatus.BAD_REQUEST, "Usuario o password incorrecto"); }
 */