package com.repo.utils;

import com.repo.db.model.User;
import io.jsonwebtoken.Claims;

public class AuthUser {
  public User user;
  public String token;

  public User getUser () {
    return this.user;
  }

  public int getUserType() {
    return this.user.getUserType();
  }

  public String getUsername() {
    return this.user.getUsername();
  }

  public int getId () {
    return this.user.getId();
  }

  public AuthUser(User user) {
    this.user = user;
  }

  public AuthUser(User user, String token) {
    user.setPassword(null);
    this.user = user;
    this.token = token;
  }

  public AuthUser(Claims claims) {
    this.user = new User();
    this.user.setId(claims.get("id", Integer.class));
    this.user.setUsername(claims.get("username", String.class));
    this.user.setProfileImage(claims.get("profileImage", String.class));
    this.user.setUserType(claims.get("type", Integer.class));
  }

  public AuthUser(Claims claims, String token) {
    this.token = token;
    this.user = new User();
    this.user.setId(claims.get("id", Integer.class));
    this.user.setUsername(claims.get("username", String.class));
    this.user.setProfileImage(claims.get("profileImage", String.class));
    this.user.setUserType(claims.get("type", Integer.class));
  }
}
