package com.repo.utils;

import com.repo.db.model.User;

public interface Methods<T> {
  public T beforeUpdate(Integer id, User user);

  public T beforeCreate(User user);

  public T validation();
  /*
   *
   *
   */
}
