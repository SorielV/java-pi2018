package com.repo.utils;

public class PermissionException extends Exception {
  public PermissionException() {
    super("Este usuario no puede realizar esa accion");
  }

  public PermissionException(String message) {
    super(message);
  }

}
