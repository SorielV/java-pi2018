/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.repo.utils;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Clemente Soriel Vallejo Mejia <xxsorielxx@gmail.com>
 */
public class IP {
  /**
   * Regresa la IP de la Solicititud
   *
   * @param request
   * @return
   */
  public static String getClientIpAddress(HttpServletRequest req) {
    String ip;
    for (String header : IP_HEADER_CANDIDATES) {
      ip = req.getHeader(header);
      if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
        return ip;
      }
    }
    return req.getRemoteAddr();
  }

  private static final String[] IP_HEADER_CANDIDATES = {"X-Forwarded-For", "Proxy-Client-IP", "WL-Proxy-Client-IP",
          "HTTP_X_FORWARDED_FOR", "HTTP_X_FORWARDED", "HTTP_X_CLUSTER_CLIENT_IP", "HTTP_CLIENT_IP", "HTTP_FORWARDED_FOR",
          "HTTP_FORWARDED", "HTTP_VIA", "REMOTE_ADDR"};
}
