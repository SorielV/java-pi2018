package com.repo.utils;

import io.jsonwebtoken.JwtException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RequestHandle<T> {
  public RequestHandle(T repository, HttpServletRequest req, HttpServletResponse res) {
    this.repository = repository;
    this.req = req;
    this.res = res;
  }

  protected boolean handleAuth(boolean admin, boolean ex)
          throws PermissionException, UnauthenticatedException, JwtException {
    user = Auth.getReqUser(req);

    if (user == null) {
      if (ex)
        throw new JwtException("Token Perdido o Invalido");
      return false;
    }

    if (admin) {
      if (user.getUserType() != 1) {
        if (ex)
          throw new PermissionException();
        return false;
      }
    }

    return true;
  }

  protected boolean handleAuthCookieBase(boolean admin, boolean ex)
          throws PermissionException, UnauthenticatedException {
    this.user = Auth.isAuth(this.req);
    boolean isAuth = this.user != null;
    if (ex) {
      if (!isAuth) {
        throw new UnauthenticatedException();
      } else {
        if (admin) {
          System.out.println(this.user.getUsername() + " " + this.user.getUserType());
          if (this.user.getUserType() != 1)
            throw new PermissionException();
        }
      }
    }
    return isAuth;
  }

  protected T repository;
  protected AuthUser user;
  protected HttpServletRequest req;
  protected HttpServletResponse res;
}