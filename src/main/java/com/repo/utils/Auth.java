package com.repo.utils;

import com.repo.db.model.User;
import io.jsonwebtoken.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.util.Date;

/**
 * @author Clemente Soriel Vallejo Mejia <xxsorielxx@gmail.com>
 */
public class Auth {
  /**
   * @param req
   * @return Usuario
   */
  public static AuthUser isAuth(HttpServletRequest req) {
    HttpSession session = req.getSession(false);
    if (session != null) {
      return (AuthUser) session.getAttribute("user");
    } else {
      return null;
    }
  }

  public static String getReqToken(HttpServletRequest req) {
    String authHeader = req.getHeader("Authorization"); // xx-auth-token, ..., etc
    if (authHeader == null || !authHeader.startsWith("Bearer ")) { // 'Bearer ' => 7
      return null;
    }
    return authHeader.substring(7);
  }

  public static AuthUser getReqUser(HttpServletRequest req) {
    final String token = getReqToken(req);
    if (token == null || token.trim() == "") {
      //throw new JwtException("Token Perdido o Invalido");
      return null;
    }

    AuthUser user = null;
    try {
      Jws<Claims> jws = Jwts.parser().setSigningKey(Static.jwtSecret.getBytes("UTF-8")).parseClaimsJws(token);
      user = new AuthUser(jws.getBody(), token);
    } catch (Exception x) {
      return null;
    }

    return user;
  }

  public static AuthUser getReqUserEx(HttpServletRequest req) throws UnsupportedEncodingException, JwtException {
    final String token = getReqToken(req);
    if (token == "" || token == null) {
      throw new JwtException("Token Perdido o Invalido");
    }

    Jws<Claims> jws = Jwts.parser().setSigningKey(Static.jwtSecret.getBytes("UTF-8")).parseClaimsJws(token);
    return new AuthUser(jws.getBody());
  }

  public static AuthUser getReqUser(String token) throws UnsupportedEncodingException, JwtException {
    Jws<Claims> jws = Jwts.parser().setSigningKey(Static.jwtSecret.getBytes("UTF-8")).parseClaimsJws(token);
    return new AuthUser(jws.getBody());
  }

  public static String generateJWT(User user) throws UnsupportedEncodingException {
    String jwt = Jwts.builder().setSubject("user")
      .setExpiration(new Date(System.currentTimeMillis() + Static.jwtExpirationTime))
      .setIssuer("user")
      .claim("id", user.getId())
      .claim("username", user.getUsername())
      .claim("type", user.getUserType())
      .claim("profileImage", user.getProfileImage())
      .signWith(SignatureAlgorithm.HS256, Static.jwtSecret.getBytes("UTF-8")).compact();
    return jwt;
  }
}
