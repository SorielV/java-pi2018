package com.repo.utils;

import com.repo.db.model.User;
import io.jsonwebtoken.JwtException;
import org.springframework.boot.context.config.ResourceNotFoundException;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

public abstract class BasicCRUD<T extends Methods, ID extends Serializable> {
  public BasicCRUD(CrudRepository<T, ID> repository, HttpServletRequest req, HttpServletResponse res) {
    this.req = req;
    this.res = res;
    this.repository = repository;
  }

  protected boolean handleAuth(boolean admin, boolean ex)
          throws PermissionException, UnauthenticatedException, JwtException {
    user = Auth.getReqUser(req);

    if (user == null) {
      if (ex)
        throw new JwtException("Token Perdido o Invalido");
      return false;
    }

    if (admin) {
      if (user.getUserType() != 1) {
        if (ex)
          throw new PermissionException();
        return false;
      }
    }

    return true;
  }

  @GetMapping
  @ResponseStatus(code = HttpStatus.OK)
  public Response getAll() throws UnauthenticatedException, PermissionException {
    return new Response(req, HttpStatus.OK, repository.findAll());
  }

  @GetMapping(value = "/{id}")
  @ResponseStatus(code = HttpStatus.OK)
  public Response getById(@PathVariable("id") ID id) throws UnauthenticatedException, PermissionException {
    return new Response(req, HttpStatus.OK, (T) repository.findOne(id));
  }

  @PostMapping
  @ResponseStatus(code = HttpStatus.CREATED)
  public Response create(@RequestBody final T model)
          throws IllegalArgumentException, UnauthenticatedException, PermissionException {
    handleAuth(true, true);
    return new Response(req, HttpStatus.CREATED, repository.save((T) model.beforeCreate(user.getUser())));
  }

  @PutMapping(value = "/{id}")
  @ResponseStatus(code = HttpStatus.OK)
  public Response update(@PathVariable("id") int id, @RequestBody final T commit)
          throws IllegalArgumentException, UnauthenticatedException, PermissionException {
    handleAuth(true, true);
    return new Response(req, HttpStatus.OK, (T)repository.save((T) commit.beforeUpdate(id, user.getUser())));
  }

  @DeleteMapping(value = "/{id}")
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public Response delete(@PathVariable("id") ID id)
          throws ResourceNotFoundException, UnauthenticatedException, PermissionException {
    handleAuth(true, true);
    repository.delete(id);
    return new Response(req, HttpStatus.NO_CONTENT, null);
  }

  protected CrudRepository<T, ID> repository;
  protected AuthUser user;
  protected HttpServletRequest req;
  protected HttpServletResponse res;
}

/*
 * protected boolean handleAuthCookieBase(boolean admin, boolean ex) throws
 * PermissionException, UnauthenticatedException { this.user =
 * Auth.isAuth(this.req); boolean isAuth = this.user != null; if (ex) { if
 * (!isAuth) { throw new UnauthenticatedException(); } else { if (admin) {
 * System.out.println(this.user.getUsername() + " " + this.user.getUserType());
 * if (this.user.getUserType() != 1) throw new PermissionException(); } } }
 * return isAuth; }
 */