package com.repo.utils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Clemente Soriel Vallejo Mejia <xxsorielxx@gmail.com>
 */
public class Response {
  /**
   * Crea un objeto Reponse
   *
   * @param url
   * @param status
   * @param data
   */
  public Response(String url, Integer status, Object data) {
    this.url = url;
    this.status = status;
    this.data = data;
  }

  /**
   * Crea un objeto Reponse
   *
   * @param req
   * @param status
   * @param data
   */
  public Response(HttpServletRequest req, Integer status, Object data) {
    StringBuilder requestURL = new StringBuilder(req.getRequestURL().toString());
    this.url = requestURL.toString();
    this.status = status;
    this.data = data;
  }

  public Response(HttpServletRequest req, HttpStatus httpStatus, Object data) {
    StringBuilder requestURL = new StringBuilder(req.getRequestURL().toString());
    this.url = requestURL.toString();
    this.status = httpStatus.value();
    this.data = data;
  }

  public String getUrl() {
    return url;
  }

  public Response setStatus(Integer status) {
    this.status = status;
    return this;
  }

  public Response setUrl(String url) {
    this.url = url;
    return this;
  }

  public Response setUrl(HttpServletRequest req) {
    StringBuilder requestURL = new StringBuilder(req.getRequestURL().toString());
    this.url = requestURL.toString();
    return this;
  }

  public Integer status = 200;
  public Object data = null;
  public String url = null;
}
