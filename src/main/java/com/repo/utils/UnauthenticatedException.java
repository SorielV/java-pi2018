package com.repo.utils;

public class UnauthenticatedException extends Exception {
  public UnauthenticatedException() {
    super("No ha sido autentifiado");
  }

  public UnauthenticatedException(String message) {
    super(message);
  }

}
