create or replace table if not exists Repos.Author
(
  id        int auto_increment
    primary key,
  firstName varchar(45)                        not null,
  lastName  varchar(45)                        not null,
  createdAt datetime default CURRENT_TIMESTAMP null,
  updatedAt datetime                           null
  on update CURRENT_TIMESTAMP
);

create or replace table if not exists Repos.CatTopics
(
  id          tinyint auto_increment
    primary key,
  text        varchar(25)  not null,
  description varchar(200) null
);

create or replace table if not exists Repos.CatTypes
(
  id          tinyint auto_increment
    primary key,
  text        varchar(25)  not null,
  description varchar(200) null
);

create or replace table if not exists Repos.User
(
  id            mediumint auto_increment,
  username      varchar(20)                        not null,
  password      varchar(100)                       null
  comment 'bcrypt',
  firstName     varchar(45)                        not null,
  lastName      varchar(45)                        not null,
  birthDate     date                               not null,
  email         varchar(45)                        not null,
  profileImage  varchar(80)                        null,
  userType      smallint(2) default '0'            not null,
  recoveryToken varchar(45)                        null,
  emailToken    varchar(45)                        null,
  createdAt     datetime default CURRENT_TIMESTAMP null,
  updatedAt     datetime                           null
  on update CURRENT_TIMESTAMP,
  primary key (id, username),
  constraint User_username_uindex
  unique (username)
);

create or replace table if not exists Repos.Repository
(
  id          int auto_increment
    primary key,
  username    varchar(20)                        not null,
  idUser      mediumint                          not null,
  title       varchar(45)                        not null,
  description varchar(400)                       null,
  file        varchar(80)                        null,
  url         varchar(80)                        not null,
  topic       smallint(6)                        not null,
  type        smallint(6)                        not null,
  tags        varchar(200)                       null,
  image       varchar(80)                        null,
  createdAt   datetime default CURRENT_TIMESTAMP null,
  visibility  tinyint                            not null,
  updatedAt   datetime                           null
  on update CURRENT_TIMESTAMP,
  userId      int                                null,
  constraint fk_Repository_User
  foreign key (idUser, username) references User (id, username)
);

create or replace table if not exists Repos.Favorite
(
  id           int auto_increment
    primary key,
  idRepository int                                not null,
  type         tinyint                            not null,
  createdAt    datetime default CURRENT_TIMESTAMP null,
  username     varchar(20)                        not null,
  userId       mediumint                          not null,
  constraint fk_Favorite_Repository
  foreign key (idRepository) references Repository (id),
  constraint fk_Favorite_User
  foreign key (userId, username) references User (id, username)
);

create or replace table if not exists Repos.Likes
(
  id           int auto_increment
    primary key,
  type         smallint(6) default '1'            null,
  idRepository int                                not null,
  username     varchar(20)                        not null,
  userId       mediumint                          not null,
  createdAt    datetime default CURRENT_TIMESTAMP null,
  constraint fk_Starts_Repository
  foreign key (idRepository) references Repository (id),
  constraint fk_Starts_User
  foreign key (userId, username) references User (id, username)
);

create or replace table if not exists Repos.RepoAuthor
(
  id           int auto_increment
    primary key,
  idAuthor     int                                null,
  idRepository int                                null,
  createdAt    datetime default CURRENT_TIMESTAMP null,
  constraint fk_RepositoryAuthor_Author
  foreign key (idAuthor) references Author (id),
  constraint fk_RepositoryAuthor_Repository
  foreign key (idRepository) references Repository (id)
);

create or replace table if not exists Repos.RepoComment
(
  id           int auto_increment
    primary key,
  idRepository int                                not null,
  username     varchar(20)                        not null,
  idUser       mediumint                          not null,
  comment      varchar(255)                       not null,
  createdAt    datetime default CURRENT_TIMESTAMP null,
  updatedAt    datetime                           null,
  constraint fk_RepositoryComment_Repository
  foreign key (idRepository) references Repository (id),
  constraint fk_RepositoryComment_User
  foreign key (idUser, username) references User (id, username)
);

create or replace table if not exists Repos.Users
(
  id            int auto_increment
    primary key,
  birthDate     varchar(255) null,
  createdAt     varchar(255) null,
  email         varchar(255) null,
  emailToken    varchar(255) null,
  firstName     varchar(255) null,
  lastName      varchar(255) null,
  password      varchar(255) null,
  profileImage  varchar(255) null,
  recoveryToken varchar(255) null,
  updatedAt     varchar(255) null,
  userType      int          null,
  username      varchar(255) null
);

create or replace view if not exists Repos.ViewRepo as
  select `Repo`.`id`                                AS `id`,
         `Repo`.`title`                             AS `title`,
         `Repo`.`url`                               AS `url`,
         `Repo`.`topic`                             AS `topic`,
         `Repo`.`type`                              AS `type`,
         `Repo`.`createdAt`                         AS `createdAt`,
         `Repo`.`tags`                              AS `tags`,
         `Repo`.`username`                          AS `username`,
         `Repo`.`userId`                            AS `userId`,
         `Repo`.`visibility`                        AS `visibility`,
         `Repo`.`file`                              AS `file`,
         `Repo`.`image`                             AS `image`,
         `Repo`.`description`                       AS `description`,
         (select group_concat(`Repos`.`Author`.`lastName` separator ' | ')
          from (`Repos`.`RepoAuthor` `R` join `Repos`.`Author` on ((`Repos`.`Author`.`id` = `R`.`idAuthor`)))
          where (`R`.`idRepository` = `Repo`.`id`)) AS `authors`
  from `Repos`.`Repository` `Repo`;

create or replace view if not exists Repos.ViewRepoAuthor as
  select `R`.`id`           AS `id`,
         `A`.`id`           AS `idAuthor`,
         `R`.`idRepository` AS `idRepository`,
         `A`.`firstName`    AS `firstName`,
         `A`.`lastName`     AS `lastName`,
         `R`.`createdAt`    AS `createdAt`
  from (`Repos`.`RepoAuthor` `R` join `Repos`.`Author` `A` on ((`R`.`idAuthor` = `A`.`id`)));

create or replace view if not exists Repos.ViewRepositoryComment as
  select `RC`.`id`           AS `id`,
         `RC`.`idRepository` AS `idRepository`,
         `RC`.`username`     AS `username`,
         `U`.`profileImage`  AS `profileImage`,
         `RC`.`comment`      AS `comment`,
         `RC`.`createdAt`    AS `createdAt`,
         `RC`.`updatedAt`    AS `updatedAt`
  from (`Repos`.`RepoComment` `RC` join `Repos`.`User` `U` on (((`RC`.`idUser` = `U`.`id`) and
                                                                (`RC`.`username` = `U`.`username`))));


