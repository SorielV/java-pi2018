# PI2018

Disculpen e estado ocupado es una version vieja por tanto no tiene ciertas caractisticas.

# Herramientas
    - Spring-boot
    - Mysql (MariaDB)
    - jsonwebtoken

# Caracteristicas
    - CRUD
        - Usuario
        - Catalgos
            - Tipos
            - Temas
        - Repositorios
    - Autentificaion JWT/Cookie
    - Hooks { beforeUpdate, beforeCrate, validation }
    - Staitc Files Handle
    - Exception Handle
    - API Generica Basica

Requerimientos:
```
Importar base de datos ('./db')
Credenciales [Base de Datos]  src/main/resources/application.yml
Cliente (SPA) src/main/webapp/resources/ (Default)
Cliente (JPA) src/main/resources/ (Cambiar HTMLResolver)
```

Dev
```
mvn package
java -jar ./target/java -jar ./target/repo.io-0.9.1.jar
```

> Quiza en unos dias (3 maybe) haga cambios de estructura y actualize a la version actual por el momento les compartire eso.

